<?php

namespace Drupal\optio_dentistry\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Optio Dentistry video library
 *
 * @Block(
 *   id = "optio_dentistry_library",
 *   admin_label = @Translation("Dental video library"),
 * )
 */

class OptioDentistryLibraryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => '<div class="optio-library"></div>',
      '#attached' => array(
        'library' =>  array(
          'optio_dentistry/optio.api'
        )
      )
    );
  }

}
