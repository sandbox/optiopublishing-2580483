<?php

namespace Drupal\optio_dentistry\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Insert Optio Dentistry content
 *
 * @Filter(
 *   id = "filter_optio_dentistry",
 *   title = @Translation("Optio Dentistry"),
 *   description = @Translation("Inserts Optio Dentistry content by replacing [optio:video=id] tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterOptioDentistry extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    // Search for [optio:controls=id] tags
    if (preg_match_all("/\[optio:([^\]=]+)+=?([^\]=]+)?\]/", $text, $match)) {
      $tags = array();
      $replacements = array();
      foreach ($match[1] as $key => $value) {
        $tags[] = $match[0][$key];
        $control = $match[1][$key];
        $parameter = isset($match[2][$key]) ? $match[2][$key] : '';

        // Render content
        $content = $this->renderControl($control, $parameter);
        $replacements[] = render($content);
      }

      // Replace tags
      $text = str_replace($tags, $replacements, $text);
      $result->setProcessedText($text);
      return $result;
    }

    return $result;
  }

  /**
   * Render control
   */
  private function renderControl($control = 'video_library', $parameter = '') {

    // Return render array
    switch ($control) {

      // Video library
      case 'video-library':
      case 'library':
        return array(
          '#markup' => "<div class=\"optio-library\" data-filter=\"$parameter\"></div>",
          '#attached' => array(
            'library' =>  array(
              'optio_dentistry/optio.api'
            )
          )
        );

      // Stand-alone video player
      case 'video':
      case 'video-player':
        if (empty($parameter)) {
          return array();
        } else {
          return array(
            '#markup' => "<div class=\"optio-video\" data-video-id=\"$parameter\"></div>",
            '#attached' => array(
              'library' =>  array(
                'optio_dentistry/optio.api'
              )
            )
          );
        }

      // Thumbnail link to lightbox
      case 'thumbnail':
      case 'thumbnail-link':
        if (empty($parameter)) {
          return array();
        } else {
          return array(
            '#markup' => "<div class=\"optio-thumbnail\" data-video-id=\"$parameter\"></div>",
            '#attached' => array(
              'library' =>  array(
                'optio_dentistry/optio.api'
              )
            )
          );
        }

      // Return empty array
      default:
        return array();
    }
  }

}
